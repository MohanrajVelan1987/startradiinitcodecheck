package com.example.code.check.startradiinitcodecheck;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class ActivityB extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("flowActivitLifeB","onCreate");
        setContentView(R.layout.activity_b);
    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d("flowActivitLifeB","onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("flowActivitLifeB","onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("flowActivitLifeB","onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("flowActivitLifeB","onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("flowActivitLifeB","onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("flowActivitLifeB","onDestroy");
    }
}
