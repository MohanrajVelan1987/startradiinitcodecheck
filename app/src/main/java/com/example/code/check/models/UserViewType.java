package com.example.code.check.models;

import java.io.Serializable;

/**
 * Created by senthil on 04-05-2019.
 */

public class UserViewType implements Serializable{
    public static  final  int VIEW_TYPE_HEADER=1;
    public static  final  int VIEW_TYPE_SUB_HEADER=2;
    public static  final  int VIEW_TYPE_ITEM=3;
    private int viewType;

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }
}
