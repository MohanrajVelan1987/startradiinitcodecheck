package com.example.code.check.startradiinitcodecheck;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.code.check.fragment.BlankFragmentA;
import com.example.code.check.fragment.BlankFragmentB;

public class ActivityA extends AppCompatActivity implements BlankFragmentA.OnFragmentInteractionListener,BlankFragmentB.OnFragmentInteractionListener {

    private Button mBtnClick;
    private RelativeLayout container;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("flowActivitLifeA","onCreate");
        setContentView(R.layout.activity_activity);
        mBtnClick=findViewById(R.id.btnClick);
        container=findViewById(R.id.container);
        mBtnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(ActivityA.this,ActivityB.class);
//                startActivity(intent);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container,new BlankFragmentA(), BlankFragmentA.TAG)
                        // Add this transaction to the back stack
                        //.addToBackStack(null)
                        .commit();

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("flowActivitLifeA","onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("flowActivitLifeA","onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("flowActivitLifeA","onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("flowActivitLifeA","onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("flowActivitLifeA","onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("flowActivitLifeA","onDestroy");
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void gotoNext() {
        Toast.makeText(ActivityA.this,"hai",Toast.LENGTH_LONG).show();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,new BlankFragmentB(), BlankFragmentA.TAG)
                // Add this transaction to the back stack
                .addToBackStack(null)
                .commit();
    }
}
