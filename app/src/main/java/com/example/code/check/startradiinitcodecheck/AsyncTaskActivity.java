package com.example.code.check.startradiinitcodecheck;

import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.code.check.asyncs.AsyncTaskExample;
import com.example.code.check.fragment.RetainedFragment;
import com.example.code.check.interfaces.HandlerBridgeActivity;

public class AsyncTaskActivity extends AppCompatActivity implements HandlerBridgeActivity {
    ImageView imageView = null;
RetainedFragment retainedFragment;
TextView txtBitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task);
        Button button = findViewById(R.id.btnAsync);
        imageView = findViewById(R.id.imgAsync);
        txtBitmap=findViewById(R.id.txtBitmap);
        FragmentManager fragmentManager=getFragmentManager();
        retainedFragment= (RetainedFragment) fragmentManager.findFragmentByTag(RetainedFragment.TAG);
        if(retainedFragment==null)
        {
            retainedFragment=new RetainedFragment();
            fragmentManager.beginTransaction().add(retainedFragment,RetainedFragment.TAG).commit();
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AsyncTaskExample asyncTask=new AsyncTaskExample(AsyncTaskActivity.this);
//                asyncTask.execute("https://www.tutorialspoint.com/images/tp-logo-diamond.png");
                retainedFragment.execute(AsyncTaskActivity.this,"https://www.tutorialspoint.com/images/tp-logo-diamond.png");
            }
        });
    }

    public void getImage(Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
    }

    @Override
    public void getResult(Bitmap bitmap) {
        Log.d("CheckAsyncTask","getBitmap");
        try{
            txtBitmap.setText(bitmap+"");
            imageView.setImageBitmap(bitmap);
            Log.d("CheckAsyncTask1","with in bitmap null"+bitmap+"");
            if(bitmap==null)
            {
                Log.d("CheckAsyncTask1","with in bitmap null");
            }else {
                Log.d("CheckAsyncTask1","with in bitmap not null");
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        Log.d("CheckAsyncTask","getBitmapafter");
    }
}
