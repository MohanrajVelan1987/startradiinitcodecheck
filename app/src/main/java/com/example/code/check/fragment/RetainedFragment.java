package com.example.code.check.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;

import com.example.code.check.asyncs.AsyncTaskExample;
import com.example.code.check.interfaces.HandlerBridgeActivity;

/**
 * Created by senthil on 18-02-2019.
 */

public class RetainedFragment extends Fragment {

    public static  String TAG="RetainedFragmentTag";
    private Bitmap mBitmap;
    AsyncTaskExample asyncTaskExample;
    HandlerBridgeActivity handlerBridgeActivity;
    private boolean isExequteAsynckTask = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // The key to making data survive runtime configuration changes.
        setRetainInstance(true);
    }

    public Bitmap getData() {
        return this.mBitmap;
    }

    public void setData(Bitmap bitmapToRetain) {
        this.mBitmap = bitmapToRetain;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        HandlerBridgeActivity handlerBridgeActivity = (HandlerBridgeActivity) activity;
        Log.d("asyncFragment","with in onattach");
        if (asyncTaskExample != null) {
            Log.d("asyncFragment","with in onattach");

            asyncTaskExample.updateHandlerBridgeActivity(handlerBridgeActivity);
        }
    }

    public void cancel()
    {
        if(asyncTaskExample!=null && !asyncTaskExample.isCancelled())
        {
           isExequteAsynckTask=false;
           asyncTaskExample.cancel(true);
        }
    }

    public  void  execute(Context context, String url)
    {
        cancel();
        isExequteAsynckTask=true;
        asyncTaskExample=new AsyncTaskExample(context);
        asyncTaskExample.execute(url);
    }
}
