package com.example.code.check.startradiinitcodecheck;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class RootingActivity extends AppCompatActivity {

    private Button mMultipleViewBtn;
    private Button mExpandViewBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooting);
        mMultipleViewBtn=findViewById(R.id.multiple_view_btn);
        mExpandViewBtn=findViewById(R.id.expand_view_btn);
        mMultipleViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RootingActivity.this,MultipleViewTypeActivity.class);
                startActivity(intent);
            }
        });
        mExpandViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RootingActivity.this,ExpandableViewTypeActivity.class);
                startActivity(intent);
            }
        });

    }
}
