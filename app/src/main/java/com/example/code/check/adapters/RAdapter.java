package com.example.code.check.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
//hai

import com.example.code.check.models.RModel;
import com.example.code.check.startradiinitcodecheck.DynamicViewActivity;
import com.example.code.check.startradiinitcodecheck.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by senthil on 13-02-2019.
 */

public class RAdapter extends RecyclerView.Adapter<RAdapter.ViewHolder> {

    private static final String TAG = RAdapter.class.getSimpleName();

    private Context context;
    private DynamicViewActivity dynamicViewActivity;
    private View.OnClickListener onClickListener;
    public List<RModel> list;
    private OnItemClickListener onItemClickListener;

    public RAdapter(Context context, List<RModel> list, OnItemClickListener onItemClickListener) {
        this.context = context;
        dynamicViewActivity = (DynamicViewActivity) context;
        this.onClickListener = (View.OnClickListener) context;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }

    public void setListValue(RModel values) {
        int position = values.getPositionValue();
        list.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, list.size());
//        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        // Todo Butterknife bindings
        TextView mTV;
        ImageView mImgAdd;
        ImageView mImgRemove;
        EditText mEdtValue;

        public ViewHolder(View itemView) {
            super(itemView);
            mTV = itemView.findViewById(R.id.text);
            mEdtValue = itemView.findViewById(R.id.edtValue);
            mImgRemove = itemView.findViewById(R.id.imgRemove);
            mImgAdd = itemView.findViewById(R.id.imgAdd);

        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.inflator_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final RModel item = list.get(holder.getAdapterPosition());
        Log.d("hai",item.toString()+" " +position);
        //Todo: Setup viewholder for item
        holder.mImgAdd.setTag(item);
        holder.mTV.setText(item.getName());
        holder.mEdtValue.setText(item.getEditTextValue());
        holder.mImgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                try {
                    RModel rModel = new RModel();
                    rModel.setEditTextValue("");
                    item.setRemoveFlag(true);
                    list.add(position + 1, rModel);
                    Log.d("hai",list.toString());
                    notifyItemChanged(position);
                    notifyItemInserted(position + 1);
                  //  dynamicViewActivity.updatedataChanged();
                } catch (ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        });
//        if (position == 0 && position == getItemCount() - 1) {
//            item.setRemoveFlag(false);
//        }
        if (list.size() >= 1) {
            if (position == list.size() - 1) {
                item.setRemoveFlag(false);
            }
        }
        if (list.size() > 1 && position == 5 - 1) {
            item.setRemoveFlag(true);
        }

        if (item.isRemoveFlag() == false) {
//            holder.mEdtValue.setText("");
            holder.mImgAdd.setVisibility(View.VISIBLE);
            holder.mImgRemove.setVisibility(View.GONE);
        } else if (item.isRemoveFlag() == true) {
            holder.mImgAdd.setVisibility(View.GONE);
            holder.mImgRemove.setVisibility(View.VISIBLE);
        }
        holder.mImgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.isRemoveFlag() == true) {
                    int currentPosition = holder.getAdapterPosition();
//                    if(currentPosition==)
//                    Toast.makeText(context, "the position is" + holder.mEdtValue.getText().toString().trim(), Toast.LENGTH_LONG).show();
//                    holder.mEdtValue.setText("");

                    Log.d("hai",list.toString());
                    Log.d("hai",currentPosition+"");
                    list.remove(currentPosition);
                    notifyItemRemoved(currentPosition);
                    Toast.makeText(context, "the position is" + position, Toast.LENGTH_LONG).show();
                    if(list.size()>1)
                    {
                        notifyItemRangeChanged(list.size()-1, list.size());
                    }
//                    if(currentPosition==0)
//                    {
//                        notifyItemRangeChanged(currentPosition, list.size());
//                    }else if (currentPosition>0)
//                    {
//                        notifyItemRangeChanged(currentPosition-1, list.size());
//                    }
                }
                //dynamicViewActivity.updatedataChanged();

            }
        });

        holder.mEdtValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                item.setEditTextValue(s.toString());
//                list.add(holder.getAdapterPosition(), item);
            }

            @Override
            public void afterTextChanged(Editable s) {
//                item.editTextValue = s.toString();
//
//                // Replace the item with the value updated
//                list.add(position, item);
//                notifyDataSetChanged();
            }
        });

    }

    @Override

    public int getItemCount() {
        return list.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

}
