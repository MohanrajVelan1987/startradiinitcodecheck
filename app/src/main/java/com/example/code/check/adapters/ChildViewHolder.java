package com.example.code.check.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by senthil on 05-05-2019.
 */

public class ChildViewHolder extends RecyclerView.ViewHolder {

    /**
     * Default constructor.
     *
     * @param itemView The {@link View} being hosted in this ViewHolder
     */
    public ChildViewHolder(View itemView) {
        super(itemView);
    }
}
