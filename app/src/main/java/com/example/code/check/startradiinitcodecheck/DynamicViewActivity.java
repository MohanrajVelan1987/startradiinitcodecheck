package com.example.code.check.startradiinitcodecheck;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.code.check.adapters.RAdapter;
import com.example.code.check.models.RModel;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DynamicViewActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.my_recycler_view)
    RecyclerView mRecyclerView;
    private List<RModel> mList;
    @BindView(R.id.btnGetValue)
    Button mBtnGetValue;
    RAdapter rAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_view);
        ButterKnife.bind(this);
        mList = new ArrayList<>();
        // create100Rows();
        if (mList != null && mList.size() == 0) {
            RModel rModel = new RModel();
            rModel.setEditTextValue("");
            mList.add(rModel);

        }

        rAdapter = new RAdapter(this, mList, new RAdapter.OnItemClickListener() {

            @Override

            public void onItemClick(int position) {

                //nothing todoo here when click on button

            }

        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.setAdapter(rAdapter);
        mBtnGetValue.setOnClickListener(this);

    }

    public void updatedataChanged() {
        if (rAdapter != null)

        {
            rAdapter.notifyDataSetChanged();
        }
    }

    /**
     * When click on 'Add button' this method will be called.
     *
     * @param view Particular View
     */

    public void addOneRow(View view) {

        addRow();

        refreshRV();

    }

    public void addOneRow() {

        addRow();

        refreshRV();

    }

    /**
     * Adding row to existing list at end.
     */

    private void addRow() {

        String name = String.valueOf(getSizeOfList());

        RModel rModel = new RModel();

        rModel.setName(name);

        mList.add(rModel);

    }

    /**
     * Creating 100 rows dynamically
     */

    private void create100Rows() {

        for (int i = 0; i < 1; i++) {

            RModel rModel = new RModel();

            rModel.setName("Row no " + i);

            mList.add(rModel);

        }

    }

    /**
     * Refreshing RecyclerView and scroll up dynamically
     */

    private void refreshRV() {

        mRecyclerView.getAdapter().notifyItemInserted(getSizeOfList());

        mRecyclerView.smoothScrollToPosition(getSizeOfList());

    }

    /**
     * Get total size of list
     *
     * @return Total size of list
     */

    private int getSizeOfList() {

        return mList.size();

    }

    private String getValues() {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < mList.size(); i++) {
            View view = mRecyclerView.getChildAt(i);
            EditText nameEditText = (EditText) view.findViewById(R.id.edtValue);
            String name = nameEditText.getText().toString();
            jsonArray.put(name);
        }

        return jsonArray.toString();
    }

    //alert for coupon failed
    public void showCouponPaymentFailedAlert(String values) {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(DynamicViewActivity.this);
        alertDialog.setCancelable(false);
        alertDialog.setTitle("List of values");
        alertDialog.setMessage(values);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    public void onClick(View v) {
        RModel rModel = (RModel) v.getTag();
        switch (v.getId()) {
//            case R.id.imgAdd:
//                if (rModel.isRemoveFlag() == false) {
//                 //   ((RAdapter)mRecyclerView.getAdapter()).setListValue(rModel);
////                    ((RAdapter)mRecyclerView.getAdapter()).notifyItemRemoved(rModel.getPositionValue());
//                    rModel.setRemoveFlag(true);
//                    addOneRow();
//                }
////                else {
////                    rModel.setRemoveFlag(true);
////                    addOneRow();
////
////                }
//                mRecyclerView.getAdapter().notifyDataSetChanged();
//                break;
            case R.id.btnGetValue:
                String values = getValues();
                showCouponPaymentFailedAlert(values);
                break;
        }
    }
}
