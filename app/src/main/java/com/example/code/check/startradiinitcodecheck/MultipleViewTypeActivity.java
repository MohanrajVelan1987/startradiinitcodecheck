package com.example.code.check.startradiinitcodecheck;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;

import com.example.code.check.adapters.MultiViewTypeAdapter;
import com.example.code.check.models.StudentHeaderValue;
import com.example.code.check.models.StudentItemValue;
import com.example.code.check.models.StudentSubHeaderValue;
import com.example.code.check.models.UserViewType;

import java.util.ArrayList;

public class MultipleViewTypeActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private ArrayList<UserViewType> userViewTypeArrayList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_view_type);
        mRecyclerView = findViewById(R.id.recyclerView);
        getListStudentsValues();
        MultiViewTypeAdapter adapter = new MultiViewTypeAdapter(userViewTypeArrayList,MultipleViewTypeActivity.this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(adapter);
    }



    private void getListStudentsValues()
    {
        userViewTypeArrayList=new ArrayList<>();
        StudentHeaderValue studentHeaderValue=new StudentHeaderValue();
        studentHeaderValue.setHeaderText("View Students grade level");
        studentHeaderValue.setViewType(UserViewType.VIEW_TYPE_HEADER);
        StudentSubHeaderValue studentSubHeaderValue1=new StudentSubHeaderValue();
        studentSubHeaderValue1.setSubHeaderText("Grade A");
        studentSubHeaderValue1.setViewType(UserViewType.VIEW_TYPE_SUB_HEADER);
        StudentSubHeaderValue studentSubHeaderValue2=new StudentSubHeaderValue();
        studentSubHeaderValue2.setSubHeaderText("Grade B");
        studentSubHeaderValue2.setViewType(UserViewType.VIEW_TYPE_SUB_HEADER);
        StudentItemValue studentItemValue1=new StudentItemValue();
        studentItemValue1.setDescriptionText("student grade A1");
        studentItemValue1.setGradeLevel("A");
        studentItemValue1.setViewType(UserViewType.VIEW_TYPE_ITEM);

        StudentItemValue studentItemValue2=new StudentItemValue();
        studentItemValue2.setDescriptionText("student grade A2");
        studentItemValue2.setGradeLevel("A");
        studentItemValue2.setViewType(UserViewType.VIEW_TYPE_ITEM);

        StudentItemValue studentItemValue3=new StudentItemValue();
        studentItemValue3.setDescriptionText("student grade A3");
        studentItemValue3.setGradeLevel("A");
        studentItemValue3.setViewType(UserViewType.VIEW_TYPE_ITEM);

        StudentItemValue studentItemValue4=new StudentItemValue();
        studentItemValue4.setDescriptionText("student grade B1");
        studentItemValue4.setGradeLevel("B");
        studentItemValue4.setViewType(UserViewType.VIEW_TYPE_ITEM);

        StudentItemValue studentItemValue5=new StudentItemValue();
        studentItemValue5.setDescriptionText("student grade B2");
        studentItemValue5.setGradeLevel("B");
        studentItemValue5.setViewType(UserViewType.VIEW_TYPE_ITEM);

        StudentItemValue studentItemValue6=new StudentItemValue();
        studentItemValue6.setDescriptionText("student grade B3");
        studentItemValue6.setGradeLevel("B");
        studentItemValue6.setViewType(UserViewType.VIEW_TYPE_ITEM);

        StudentItemValue studentItemValue7=new StudentItemValue();
        studentItemValue7.setDescriptionText("student grade b4");
        studentItemValue7.setGradeLevel("B");
        studentItemValue7.setViewType(UserViewType.VIEW_TYPE_ITEM);

        userViewTypeArrayList.add(studentHeaderValue);
        userViewTypeArrayList.add(studentSubHeaderValue1);
        userViewTypeArrayList.add(studentItemValue1);
        userViewTypeArrayList.add(studentItemValue2);
        userViewTypeArrayList.add(studentItemValue3);
        userViewTypeArrayList.add(studentSubHeaderValue2);
        userViewTypeArrayList.add(studentItemValue4);
        userViewTypeArrayList.add(studentItemValue5);
        userViewTypeArrayList.add(studentItemValue6);
        userViewTypeArrayList.add(studentItemValue7);



    }


}
