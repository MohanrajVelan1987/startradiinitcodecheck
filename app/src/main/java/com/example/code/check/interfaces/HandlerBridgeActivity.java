package com.example.code.check.interfaces;

import android.graphics.Bitmap;

/**
 * Created by senthil on 18-02-2019.
 */

public interface HandlerBridgeActivity {
    public void getResult(Bitmap bitmap);
}
