package com.example.code.check.adapters;

import android.content.Context;
import android.graphics.ColorSpace;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.code.check.models.StudentHeaderValue;
import com.example.code.check.models.StudentItemValue;
import com.example.code.check.models.StudentSubHeaderValue;
import com.example.code.check.models.UserViewType;
import com.example.code.check.startradiinitcodecheck.R;

import java.util.ArrayList;

/**
 * Created by senthil on 04-05-2019.
 */

public class MultiViewTypeAdapter extends RecyclerView.Adapter {
    private ArrayList<UserViewType> userViewTypeArrayList;
    private Context mContext;

    public MultiViewTypeAdapter(ArrayList<UserViewType> data, Context context) {
        this.userViewTypeArrayList = data;
        this.mContext = context;
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case UserViewType.VIEW_TYPE_HEADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_student_item_view, parent, false);
                return new HeaderTypeViewHolder(view);
            case UserViewType.VIEW_TYPE_SUB_HEADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_header_student_item_view, parent, false);
                return new SubHeaderTypeViewHolder(view);
            case UserViewType.VIEW_TYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_item_view, parent, false);
                return new ItemTypeViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        UserViewType userViewType = userViewTypeArrayList.get(position);
        if (userViewType != null) {
            switch (userViewType.getViewType()) {
                case UserViewType.VIEW_TYPE_HEADER:
                    if(userViewType instanceof StudentHeaderValue)
                    {
                        StudentHeaderValue studentHeaderValue= (StudentHeaderValue) userViewType;
                        ((HeaderTypeViewHolder) holder).txtHeaderType.setText(studentHeaderValue.getHeaderText());
                    }
                    break;
                case UserViewType.VIEW_TYPE_SUB_HEADER:
                    if(userViewType instanceof StudentSubHeaderValue)
                    {
                        StudentSubHeaderValue studentSubHeaderValue= (StudentSubHeaderValue) userViewType;
                        ((SubHeaderTypeViewHolder) holder).txtSubHeaderType.setText(studentSubHeaderValue.getSubHeaderText());
                    }
                    break;
                case UserViewType.VIEW_TYPE_ITEM:
                    if(userViewType instanceof StudentItemValue)
                    {
                        StudentItemValue studentItemValue= (StudentItemValue) userViewType;
                        ((ItemTypeViewHolder) holder).txtItemDesc.setText(studentItemValue.getDescriptionText());
                        if(studentItemValue.getGradeLevel().equalsIgnoreCase("A"))
                        {
                            ((ItemTypeViewHolder) holder).itemRly.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Toast.makeText(mContext,"Click A level",Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return userViewTypeArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = userViewTypeArrayList.get(position).getViewType();
        switch (viewType) {
            case 1:
                return UserViewType.VIEW_TYPE_HEADER;
            case 2:
                return UserViewType.VIEW_TYPE_SUB_HEADER;
            case 3:
                return UserViewType.VIEW_TYPE_ITEM;
            default:
                return -1;
        }
    }

    public static class HeaderTypeViewHolder extends RecyclerView.ViewHolder {

        TextView txtHeaderType;

        public HeaderTypeViewHolder(View itemView) {
            super(itemView);
            this.txtHeaderType = (TextView) itemView.findViewById(R.id.header_text);
        }
    }

    public static class SubHeaderTypeViewHolder extends RecyclerView.ViewHolder {

        TextView txtSubHeaderType;

        public SubHeaderTypeViewHolder(View itemView) {
            super(itemView);
            this.txtSubHeaderType = (TextView) itemView.findViewById(R.id.sub_header_text);
        }
    }

    public static class ItemTypeViewHolder extends RecyclerView.ViewHolder {

        TextView txtItemDesc;
        RelativeLayout itemRly;

        public ItemTypeViewHolder(View itemView) {
            super(itemView);
            this.txtItemDesc = (TextView) itemView.findViewById(R.id.item_desc_text);
            this.itemRly=(RelativeLayout) itemView.findViewById(R.id.item_rly);
        }
    }


}
