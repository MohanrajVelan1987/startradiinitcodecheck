package com.example.code.check.asyncs;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.example.code.check.interfaces.HandlerBridgeActivity;
import com.example.code.check.startradiinitcodecheck.AsyncTaskActivity;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by senthil on 18-02-2019.
 */

public class AsyncTaskExample extends AsyncTask<String, String, Bitmap> {

    URL ImageUrl = null;
    InputStream is = null;
    Bitmap bmImg = null;
//    ProgressDialog p;
    Context context;
    AsyncTaskActivity asyncTaskActivity;
    private WeakReference<HandlerBridgeActivity> handlerBridgeActivityWeakReference;
    HandlerBridgeActivity handlerBridgeActivity;

    public AsyncTaskExample(Context context) {
        this.context = context;
        this.asyncTaskActivity = (AsyncTaskActivity) context;
        handlerBridgeActivity= (HandlerBridgeActivity) context;
        handlerBridgeActivityWeakReference=new WeakReference<HandlerBridgeActivity>(handlerBridgeActivity);
    }

    public  void updateHandlerBridgeActivity(HandlerBridgeActivity handlerBridgeActivity)
    {
        Log.d("CheckAsyncTask","update");
        handlerBridgeActivityWeakReference=new WeakReference<HandlerBridgeActivity>(handlerBridgeActivity);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        p = new ProgressDialog(context);
//        p.setMessage("Please wait...It is downloading");
//        p.setIndeterminate(false);
//        p.setCancelable(false);
//        p.show();
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        try {
            ImageUrl = new URL(strings[0]);
            HttpURLConnection conn = (HttpURLConnection) ImageUrl.openConnection();
            conn.setDoInput(true);
            conn.connect();
            is = conn.getInputStream();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            bmImg = BitmapFactory.decodeStream(is, null, options);
            for (int i=0;i<1000;i++)
            {
                Log.d("async",i+"");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmImg;
    }

    @Override
    protected void onPostExecute(final Bitmap bitmap) {
        super.onPostExecute(bitmap);
        final Handler handler = new Handler();
        final HandlerBridgeActivity handlerBridgeActivity;
        handlerBridgeActivity=handlerBridgeActivityWeakReference.get();
        Log.d("CheckAsyncTask","with in onPost");

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms

               // p.hide();
                if(handlerBridgeActivity!=null)
                {
                    Log.d("CheckAsyncTask","with in onPost not null");
                    handlerBridgeActivity.getResult(bitmap);
                    Log.d("CheckAsyncTask",bitmap+"");
                    if(bitmap==null)
                    {
                        Log.d("CheckAsyncTask","with in bitmap null");
                    }else {
                        Log.d("CheckAsyncTask","with in bitmap not null");
                    }
                }else {
                    Log.d("CheckAsyncTask","with in onPost  null");
                }

//                asyncTaskActivity.getImage(bitmap);
            }
        }, 3000);
//        if(imageView!=null) {
//            p.hide();
//            imageView.setImageBitmap(bitmap);
//        }else {
//            p.show();
//        }
    }
}
