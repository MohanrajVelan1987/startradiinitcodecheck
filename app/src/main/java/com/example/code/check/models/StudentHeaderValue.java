package com.example.code.check.models;

/**
 * Created by senthil on 04-05-2019.
 */

public class StudentHeaderValue extends UserViewType{
    private String headerText;

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }
}
