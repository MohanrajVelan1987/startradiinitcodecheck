package com.example.code.check.models;

/**
 * Created by senthil on 04-05-2019.
 */

public class StudentSubHeaderValue extends UserViewType{
    private String subHeaderText;

    public String getSubHeaderText() {
        return subHeaderText;
    }

    public void setSubHeaderText(String subHeaderText) {
        this.subHeaderText = subHeaderText;
    }
}
