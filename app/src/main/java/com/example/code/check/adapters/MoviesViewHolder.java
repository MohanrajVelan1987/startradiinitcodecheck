package com.example.code.check.adapters;

import android.view.View;
import android.widget.TextView;

import com.example.code.check.models.Movies;
import com.example.code.check.startradiinitcodecheck.R;

/**
 * Created by senthil on 05-05-2019.
 */

public class MoviesViewHolder extends ChildViewHolder {

    private TextView mMoviesTextView;

    public MoviesViewHolder(View itemView) {
        super(itemView);
        mMoviesTextView = (TextView) itemView.findViewById(R.id.tv_movies);
    }

    public void bind(Movies movies) {
        mMoviesTextView.setText(movies.getName());
    }
}