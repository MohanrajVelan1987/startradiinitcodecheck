package com.example.code.check.models;

/**
 * Created by senthil on 05-05-2019.
 */

public class Movies {
    private String mName;

    public Movies(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }
}
