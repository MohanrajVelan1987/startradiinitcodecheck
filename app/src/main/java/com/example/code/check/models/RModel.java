package com.example.code.check.models;

/**
 * Created by senthil on 13-02-2019.
 */

public class RModel {
    String name;
    boolean removeFlag;
    int positionValue;
    public String editTextValue = ""; // Default EditText Value


    public int getPositionValue() {
        return positionValue;
    }

    public void setPositionValue(int positionValue) {
        this.positionValue = positionValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRemoveFlag() {
        return removeFlag;
    }

    public void setRemoveFlag(boolean removeFlag) {
        this.removeFlag = removeFlag;
    }

    public String getEditTextValue() {
        return editTextValue;
    }

    public void setEditTextValue(String editTextValue) {
        this.editTextValue = editTextValue;
    }

    @Override
    public String toString() {
        return "RModel{" +
                "removeFlag=" + removeFlag +
                ", editTextValue='" + editTextValue + '\'' +
                '}';
    }
}
