package com.example.code.check.models;

/**
 * Created by senthil on 04-05-2019.
 */

public class StudentItemValue extends UserViewType{
    private String descriptionText;
    private String gradeLevel;

    public String getDescriptionText() {
        return descriptionText;
    }

    public void setDescriptionText(String descriptionText) {
        this.descriptionText = descriptionText;
    }

    public String getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(String gradeLevel) {
        this.gradeLevel = gradeLevel;
    }
}
